using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fading_object : MonoBehaviour
{
    public float maxTime;
    private float curTime = 0;
    // Update is called once per frame
    void Update()
    {
        curTime += Time.deltaTime;
        if (curTime > maxTime) Destroy(gameObject);

    }
}
