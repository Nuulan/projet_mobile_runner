using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Death_Zone : MonoBehaviour
{

    [SerializeField]
    public GameObject briser;

    private bool destruction = false;
    private float curTime = 0;

    void Update()
    {
        checkDestroy();
    }

    void checkDestroy()
    {
        if(destruction)
        {
            curTime += Time.deltaTime;
            if(curTime >= 3.0f)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Instantiate(briser, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
            Destroy(collision.gameObject);
            destruction = true;
            Handheld.Vibrate();
        }
    }
}
