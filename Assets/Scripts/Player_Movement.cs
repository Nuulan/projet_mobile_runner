using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_Movement : MonoBehaviour
{
    public Material mat;
    [SerializeField]
    public float speed;
    [SerializeField]
    public float jump;
    [SerializeField]
    public float fallMultiplier;
    [SerializeField]
    public GameObject casser;

    public GameObject wall_break;

    public int Score = 0;

    private Rigidbody rb;
    private Vector3 movement;
    private float acceleration = 0.0f;
    private bool newJump;
    private float curTime = 0f;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    void Update()
    {
        MovePlayer();
        ColorPlayer();
    }

    void MovePlayer()
    {
        rb.velocity = new Vector3(0f, rb.velocity.y, speed);
        if (newJump && (Input.GetKeyDown(KeyCode.Space) | (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)))
        {
            rb.AddForce(Vector3.up * jump, ForceMode.Impulse);
            newJump = false;
        }
        if (rb.velocity.y < 0)
        {
            rb.velocity -= Vector3.down * Physics2D.gravity.y * Time.deltaTime;
        }
    }

    void ColorPlayer()
    {
        if (newJump)
        {
            mat.color = Color.blue;
        }
        else
        {
            mat.color = Color.red;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Ground")
        {
            newJump = true;
        }
        if (collision.gameObject.tag == "JumpPad")
        {
            rb.AddForce(new Vector3(0f,17f,10f), ForceMode.Impulse);
            newJump = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Coin")
        {
            Destroy(other.gameObject);
            Score += 10;
        }
    }

    private void OnDestroy()
    {
        mat.color = Color.blue;
    }
}
