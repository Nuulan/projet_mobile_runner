using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Get_Score : MonoBehaviour
{
    public TextMeshProUGUI textmesh;

    public GameObject player;

    private float points;

    void Update()
    {
        points = player.GetComponent<Player_Movement>().Score;
        textmesh.text = "Score : " + points.ToString();
    }
}
