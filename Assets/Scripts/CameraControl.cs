using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public Transform cam;

    // Update is called once per frame
    void Update()
    {
        cam.position = gameObject.transform.position + new Vector3(4.5f, 7f, -18f);
        cam.LookAt(gameObject.transform);
    }
}
